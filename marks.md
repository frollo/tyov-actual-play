# Marchi

## Ferita
Ho una ferita sempre aperta nel petto, che perde sangue tutte le notti. Devo coprirla con cura per non farmi scoprire.

## Zanne
Dopo aver respirato il fiato di Tarantasio, le mie zanne hanno smesso di ritrarsi nella mia bocca. Ora devo stare attento a quando la apro, non è più la bocca di un umano.

## Scaglie
La pelle del mio corpo si sta ricoprendo di scaglie, come quella di un serpente. Devo tenerne coperto il più possibile e muovermi al buio.
