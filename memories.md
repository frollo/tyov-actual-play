# Memorie

## Lupo tra le pecore
 * Sono tornato a Pavia troppo presto e si vede che sono un longobardo. I franchi mi trattano con superiorità, come se fossi uno degli umani che hanno conquistato. Se solo sapessero... Ho imparato da tempo a tenere segreti e a scoprire quelli degli altri. Ora mi muovo tra loro e rivendo i loro segreti a chi mi ripaga con il giusto rispetto.
 * Il mondo è cambiato di nuovo. Mentre dormivo, re Carlo è diventato Carlo il Grande, Sacro Romano Imperatore. Ho scritto un libro su quello che ho vissuto. E' pieno di errori, ma ha molto successo. Ora sono considerato un uomo di cultura.
## La guerra
 * I franchi di Re Carlo hanno invaso l'Italia e messo sotto assedio Pavia. Sono riuscito a rifugiarmi in un bosco lontano dalla guerra, ma ho potuto portare con me solo il mio diario. Un altro guerriero indossa la mia armatura ora, per difendere Pavia.
 * Ho catturato un vampiro franco. La sua mente è più debole della mia e non può disobbedire ai miei ordini. Ora siamo entrambi costretti a nutrirci di animali, ma mi sta insegnando a conoscere il mondo dei nuovi padroni di Pavia. Presto sarò pronto a tornare in città.
 * Ho continuamente fame. Senza gli eserciti accampati qua attorno avrei già svuotato la foresta di animali, ma gli uomini danno più soddisfazione. Adoro inseguirli nella notte, con le zanne di fuori.
## Il demone
 * Durante un viaggio ho incontrato Tarantasio, un demone delle paludi. Il suo fiato uccide gli umani, ma non ha alcun effetto su di me. Da quando l'ho incontrato, però, assomiglio sempre di più a lui.
 * Non ho più rivisto Tarantasio, ma il mio corpo gli assomiglia sempre di più. Ora ho anche le scaglie, come lui. Mi chiedo se non sia da queste parti, a riversare il suo fiato maledetto su di me mentre dormo...
 * Un cavaliere cristiano ha ucciso Tarantasio. Ora porta la sua figura disegnata sullo scudo e se ne vanta davanti a tutti. Ora non saprò mai cosa sta succedendo al mio corpo o se era in qualche modo colpa sua.
## Ago e filo
 * I miei vestiti durano molto meno del mio corpo e si sporcano spesso di sangue. E non posso certo farmeli fare da qualcuno. Ho dovuto imparare a cucire da solo, per riparare e creare nuovi abiti. Un tempo l'idea mi avrebbe offeso, ma trovo pace in questo semplice lavoro.
 * La moda e i tessuti sono cambiati troppo. Non sono più in grado di farmi da me i vestiti senza rivelarmi per quello che sono: un relitto di un mondo passato. Ho dovuto cominciare a rubarli.
## Il risveglio
 * Mi sono addormentato per secoli. Cariberto, incapace di muoversi senza il mio permesso, è rimasto nel suo sarcofago, senza svegliarmi. Nel frattempo qualcuno ha murato la porta segreta che portava alla cantina e una nuova casa ha sostituito la mia dimora. Mi ci sono voluti giorni per liberarmi senza farmi scoprire e alla fine avevo molta fame. La famiglia che viveva qui l'ha saziata. Il mio diario si è putrefatto mentre dormivo, ormai è illeggibile.
 * Ho sempre meno fame e dormo sempre più a lungo. I secoli passano tra un tramonto all'altro, spesso senza che senta il bisogno di nutrirmi. A un certo punto Cariberto ha smesso di lasciare il suo sarcofago, credo non si alzerà mai più. Non è importante, non ho più bisogno di lui. Mi basta un mortale, una volta ogni qualche secolo.
